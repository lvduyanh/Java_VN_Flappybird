/*
 * 1412008
 */
package flappybird;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import pkg2dgamesframework.Objects;

/**
 *
 * @author Pisces
 */
public class Chimney extends Objects {
    private Rectangle rect;
    public boolean behindBird = false;
    
    public Chimney(int x, int y, int  w, int h) {
        super(x, y, w, h);
        rect = new Rectangle(x, y, w, h - 10);
    }
    
    public void update() {
        this.setPosX(this.getPosX() - Ground.SPEED);
        this.rect.setLocation((int)this.getPosX(), (int)this.getPosY());
    }
    
    public void Paint(Graphics2D g2, BufferedImage img) {
        g2.drawImage(img, (int) this.getPosX(), (int) this.getPosY(), null);
    }

    public Rectangle getRect() {
        return rect;
    }
    
}
