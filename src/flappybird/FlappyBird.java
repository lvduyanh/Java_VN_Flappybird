/*
 * 1412008
 */
package flappybird;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.imageio.ImageIO;
import pkg2dgamesframework.AFrameOnImage;
import pkg2dgamesframework.Animation;
import pkg2dgamesframework.GameScreen;

/**
 *
 * @author Pisces
 */
public class FlappyBird extends GameScreen{
    
    private BufferedImage birds;
    private BufferedImage backGroundImage;
    private final Animation bird_ani;
    public static float gravity = 0.20f;
    private final Bird bird;
    private final Ground ground;
    private final Chimneys chimneys;
    private int score = 0;
    private int maxScore = 0;
    
    private final int BEGIN_GAME = 0;
    private final int GAMEPLAY_SCREEN = 1;
    private final int GAMEOVER_SCREEN = 2;
    private int currentScreen = BEGIN_GAME;
    
    public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 600;
    
    public FlappyBird() {
        super(SCREEN_WIDTH, SCREEN_HEIGHT);
        this.setTitle("Flappy Bird by @lvduyanh");
        this.setResizable(false);
        
        try {
            birds = ImageIO.read(FlappyBird.class.getResource("/Assets/bird_sprite.png"));
            backGroundImage = ImageIO.read(FlappyBird.class.getResource("/Assets/bg.png"));
        } catch (IOException ex) {}
        
        bird_ani = new Animation(80);
        AFrameOnImage af = new AFrameOnImage(0, 0, 60, 60);
        bird_ani.AddFrame(af);
        af = new AFrameOnImage(60, 0, 60, 60);
        bird_ani.AddFrame(af);
        af = new AFrameOnImage(120, 0, 60, 60);
        bird_ani.AddFrame(af);
        bird = new Bird(330, 230, 50, 50);
        ground = new Ground();
        chimneys = new Chimneys();
        
        this.BeginGame();
        this.setVisible(true);
    }

    @Override
    public void GAME_UPDATE(long deltaTime) {
        switch (currentScreen) {
            case BEGIN_GAME:
                break;
            case GAMEPLAY_SCREEN:
                bird.update(deltaTime);
                if (bird.isAlive) {
                    bird_ani.Update_Me(deltaTime);
                    ground.update();
                    chimneys.update();
                    for (int i = 0; i < Chimneys.SIZE; i++) {
                        if (bird.getRect().intersects(chimneys.get(i).getRect())) {
                            bird.isAlive = false;
                            bird.getBupSound().play();
                        }
                        else if ((i%2) == 0 && !chimneys.get(i).behindBird && bird.getPosX() > chimneys.get(i).getPosX()) {
                            score++;
                            bird.getGetMoneySound().play();
                            chimneys.get(i).behindBird = true;
                        }
                    }
                }
                if ((bird.getPosY() + bird.getH()) > ground.getpY()) {
                    currentScreen = GAMEOVER_SCREEN;
                    bird.getBupSound().play();
                }
                break;
            case GAMEOVER_SCREEN:
                break;
            default:
                break;
        }
    }

    @Override
    public void GAME_PAINT(Graphics2D g2) {
        //g2.setColor(Color.decode("#b8daef"));
        //g2.fillRect(0, 0, MASTER_WIDTH, MASTER_HEIGHT);
        g2.drawImage(backGroundImage, 0, 0, this);
        chimneys.Paint(g2);
        ground.Paint(g2);
        g2.setFont(g2.getFont().deriveFont(20.0f));
        switch (currentScreen) {
            case BEGIN_GAME:
                g2.setColor(Color.red);
                g2.drawString("PRESS SPACE TO FLY", 300, 150);
                bird_ani.PaintAnims((int)bird.getPosX(), (int)bird.getPosY(), birds, g2, 0, 0);
                break;
            case GAMEPLAY_SCREEN:
                g2.setFont(g2.getFont().deriveFont(50.0f));
                g2.setColor(Color.white);
                g2.drawString("" + score, SCREEN_WIDTH/2 - 15, 86);
                if (bird.getIsFlying())
                    bird_ani.PaintAnims((int)bird.getPosX(), (int)bird.getPosY(), birds, g2, 0, -1);
                else
                    bird_ani.PaintAnims((int)bird.getPosX(), (int)bird.getPosY(), birds, g2, 0, 0.5F);
                break;
            case GAMEOVER_SCREEN:
                g2.setColor(Color.red);
                if (score > maxScore) maxScore = score;
                g2.drawString("Score: " + score + "/" + maxScore, SCREEN_WIDTH/2 - 40, 80);
                g2.drawString("PRESS ENTER TO PLAY AGAIN", 260, 150);
                bird_ani.PaintAnims((int)bird.getPosX(), (int)bird.getPosY(), birds, g2, 0, 0.5F);
                break;
            default:
                break;
        }
    }

    @Override
    public void KEY_ACTION(KeyEvent e, int Event) {
        if (Event == KEY_PRESSED) {
            switch (currentScreen) {
                case BEGIN_GAME:
                    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                        bird.fly();
                        currentScreen = GAMEPLAY_SCREEN;
                    }
                    break;
                case GAMEPLAY_SCREEN:
                    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                        if (bird.isAlive && bird.getPosY() > -180)
                            bird.fly();
                    }
                    break;
                case GAMEOVER_SCREEN:
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        currentScreen = BEGIN_GAME;
                        bird.resetBird();
                        chimneys.reset();
                        score = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    
}
