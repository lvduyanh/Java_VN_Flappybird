/*
 * 1412008
 */
package flappybird;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Pisces
 */
public class Ground {
    private BufferedImage groundImage;
    
    private int pX1, pY, pX2;
    public static final float SPEED = 3;
    
    public Ground () {
        try {
            groundImage = ImageIO.read(FlappyBird.class.getResource("/Assets/ground.png"));
        } catch (IOException ex) {}
        
        pX1 = 0;
        pY = 500;
        pX2 = pX1 + 830;
    }

    public int getpY() {
        return pY;
    }
    
    public void update() {
        pX1 -= SPEED;
        pX2 -= SPEED;
        if (pX2 < 0) pX1 = pX2 + 830;
        if (pX1 < 0) pX2 = pX1 + 830;
    }
    
    public void Paint(Graphics2D g2) {
        g2.drawImage(groundImage, pX1, pY, null);
        g2.drawImage(groundImage, pX2, pY, null);
    }
}
