/*
 * 1412008
 */
package flappybird;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.imageio.ImageIO;
import pkg2dgamesframework.QueueList;

/**
 *
 * @author Pisces
 */
public class Chimneys {
    private QueueList<Chimney> chimneys;
    private BufferedImage chimneyUp;
    private BufferedImage chimneyDown;
    
    private int chimneyW = 74;
    private int chimneyH = 400;
    private int chimneyDistanceX = 260;
    private int chimneyDistanceY = 575;
    private int startPointX = 750;
    private int startPointY = 200;
    
    public static final int SIZE = 8;
    
    public Chimneys() {
        try {
            chimneyUp = ImageIO.read(FlappyBird.class.getResource("/Assets/chimney.png"));
            chimneyDown = ImageIO.read(FlappyBird.class.getResource("/Assets/chimney_.png"));
        } catch (IOException ex) {}
        this.inIt();
    }
    
    private void inIt() {
        chimneys = new QueueList<Chimney>();
        Chimney tmpCN;
        for (int i = 0; i < this.SIZE/2; i++) {
            int rY = getRandomY();
            tmpCN = new Chimney(startPointX + chimneyDistanceX * i, rY, chimneyW, chimneyH);
            chimneys.push(tmpCN);
            tmpCN = new Chimney(startPointX + chimneyDistanceX * i, rY - chimneyDistanceY, chimneyW, chimneyH);
            chimneys.push(tmpCN);
        }
    }
    
    public void update() {
        for (int i = 0; i < this.SIZE; i++) {
            chimneys.get(i).update();
        }
        int rY = getRandomY();
        if (chimneys.get(0).getPosX() < -chimneyW) {
            Chimney cn = chimneys.pop();
            cn.setPosX(chimneys.get(chimneys.getSize()-1).getPosX() + chimneyDistanceX);
            cn.setPosY(rY);
            cn.behindBird = false;
            chimneys.push(cn);
            cn = chimneys.pop();
            cn.setPosX(chimneys.get(chimneys.getSize()-1).getPosX());
            cn.setPosY(rY - chimneyDistanceY);
            cn.behindBird = false;
            chimneys.push(cn);
        }
    }
    
    public void Paint(Graphics2D g2) {
        for (int i = 0; i < this.SIZE; i++) {
            if (i%2 == 0)
                chimneys.get(i).Paint(g2, chimneyUp);
            else
                chimneys.get(i).Paint(g2, chimneyDown);
        }
    }
    
    public Chimney get(int i) {
        if (i < this.SIZE)
            return chimneys.get(i);
        return null;
    }
    
    public void reset() {
        inIt();
    }
    
    private int getRandomY() {
        Random r = new Random();
        return ((2 + r.nextInt(10)) * 25 + startPointY);
    }
}
