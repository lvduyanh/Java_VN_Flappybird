/*
 * 1412008
 */
package flappybird;

import java.awt.Rectangle;
import pkg2dgamesframework.Objects;
import pkg2dgamesframework.SoundPlayer;

/**
 *
 * @author Pisces
 */
public class Bird extends Objects {
    private boolean isFlying = false;
    public boolean isAlive = true;
    private final float FLY_DISTANCE = -6.4f;
    private float vt = 0;
    final int saveX;
    final int saveY;
    private Rectangle rect;
    private SoundPlayer flapSound, bupSound, getMoneySound;
    
    public Bird (int x, int y, int  w, int h) {
        super(x, y, w, h);
        rect = new Rectangle(x, y, w - 10, h - 10);
        saveX = x;
        saveY = y;
        
        flapSound = new SoundPlayer(FlappyBird.class.getResource("/Assets/fap.wav"));
        bupSound = new SoundPlayer(FlappyBird.class.getResource("/Assets/fall.wav"));
        getMoneySound = new SoundPlayer(FlappyBird.class.getResource("/Assets/getpoint.wav"));
        
    }
    
    public void update(long deltatime) {
        vt += FlappyBird.gravity;
        this.setPosY(this.getPosY() + vt);
        this.rect.setLocation((int)this.getPosX(), (int)this.getPosY());
        if (vt < 0)
            isFlying = true;
        else
            isFlying = false;
    }
    
    public void fly() {
        vt = FLY_DISTANCE;
        flapSound.play();
    }

    public boolean getIsFlying() {
        return isFlying;
    }
    
    public void resetBird() {
        vt = 0;
        this.setPosY(saveY);
        isAlive = true;
    }

    public Rectangle getRect() {
        return rect;
    }

    public SoundPlayer getFlapSound() {
        return flapSound;
    }

    public SoundPlayer getBupSound() {
        return bupSound;
    }

    public SoundPlayer getGetMoneySound() {
        return getMoneySound;
    }
    
}
