/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dgamesframework;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author phamn
 */
public class Animation {
    
    private long beginTime = 0;
    
    private long mesure = 20;
    
    private ArrayList<AFrameOnImage> frames;
    private int NumOfFrame = 0;
    private int CurrentFrame = 0;
    
    public Animation(long mesure){
        this.mesure = mesure;
        frames = new ArrayList<AFrameOnImage>();
    }
    
    public void Update_Me(long deltaTime){
        if(NumOfFrame>0){
            if(deltaTime - beginTime > mesure){
                CurrentFrame++;
                if(CurrentFrame>=NumOfFrame) 
                    CurrentFrame = 0;
                beginTime = deltaTime;
            }
        }
    }
    public void AddFrame(AFrameOnImage sprite){
        frames.add(sprite);
        NumOfFrame = frames.size();
    }
    
    public void PaintAnims(int x, int y, BufferedImage image, Graphics2D g2, int anchor, float rotation){
        frames.get(CurrentFrame).Paint(x, y, image, g2, anchor, rotation);
    }
}
